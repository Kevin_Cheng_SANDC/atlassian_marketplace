FROM java:8
WORKDIR /
ADD target/marketplace-0.0.1-SNAPSHOT.jar //
EXPOSE 8080
ENTRYPOINT [ "java", "-jar", "/marketplace-0.0.1-SNAPSHOT.jar"]
