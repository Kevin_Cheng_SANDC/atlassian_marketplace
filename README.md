# Atlassian Marketplace RESTful API Microservice
***
Contains endpoints for creating, reading, and updating contacts and accounts for the new Atlassian backend service. This project provides RESTful endpoints that return application/json and also application/hal+json with Spring MVC and Spring Data.


## Technologies Used
* Java 8, Maven, Spring Boot, Spring MVC, Spring Data, Spring HAL Browser, Docker, H2 In memory Database

## Description
* Used Spring REST data for Repository interfaces to allow users to create, read, update accounts, contacts, and addresses. Added an abstraction layer over 
the database entity tables by introducing DataTransferObject POJO classes for interaction between front end clients and server. Leveraged JPA and H2 for development purposes, and added foreign key relationships for related tables. I broke down the tables into Accounts, Addresses, and Contacts to adhere to better Database design by following 1NF, 2NF, and 3NF conventions. Please see source code for more details. Deletions are disabled as per the engineering requirement. 

## How to start
* Import maven project into IntelliJ and press the green Play button. H2 In memory Database will startup, and the Facade class I added will initialize the DB with a set of dummy data. I added several different contacts for different accounts and added some contacts with no accounts to show the One-To-Many relationship.
* Can use Postman to test the RESTful endpoints. Please see attached pictures for examples of client-server responses for various CRUD operations. 

## Design Patterns Used
* Singleton (@Component, @Service annotations etc.), Facade Design Pattern (allows client class to easily add mock data to DB without knowing the intracicies.

# If time permitted
* I wanted to give an example of what a possible feature enhancment would look like, according to the marketplace UI. Therefore i added RESTful endpoints for users to retrieve the average rating for an account. It would be helpful for consumers of my API to have an OpenAPI specification i could add if time permitted. I would add end to end system tests to check tables, and performance test the application. Some things I would consider is also testing the performance tradeoffs of leveraging a Reactive Framework like Spring WebFlux if given more time, to create a fully reactive stack. I would also add Spring Actuator and secure my API with roles and JWT. I would also swap the Database with a MySql DB or host the application on AWS with AWS Beanstalk and Amazon RDS. 