package com.atlassian.marketplace.marketplace;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.atlassian.marketplace.marketplace.domain.AccountType;
import com.atlassian.marketplace.marketplace.domain.Address;
import com.atlassian.marketplace.marketplace.domain.AddressId;
import com.atlassian.marketplace.marketplace.domain.Contact;
import com.atlassian.marketplace.marketplace.service.AccountService;

/**
 * Responsible for persisting Accounts into the DB.
 */
@Component
public class AccountCreator
    implements DataCreator {
    private AccountService accountService;
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountCreator.class);

    AccountCreator(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public void createData() {
        createFirstAccount();
        createSecondAccount();
        createThirdAccount();
        createFourthAccount();
        createFifthAccount();
        LOGGER.info("created " + accountService.total() + " accounts in DB");
    }

    private void createFirstAccount() {
        AddressId.Builder addressIdBuilder = new AddressId.Builder()
                .line1("Workday Way").city("Pleasanton").state("California").country("USA");
        Set<Contact> contacts = new HashSet<>(Arrays.asList(new Contact(null, null, "Kevin Cheng", "kevin@workday.com"),
                new Contact(null, null, "Henry Kim", "henry@workday.com"),
                new Contact(null, null, "VP of Sales", "sales@workday.com")));
        accountService.createAccount(contacts, "Workday", new Address(addressIdBuilder.build()),
                AccountType.ENTERPRISE);
    }

    private void createSecondAccount() {
        AddressId.Builder addressIdBuilder = new AddressId.Builder()
                .line1("Atlassian Way").city("Austin").state("Texas").country("USA");
        Address address = new Address(addressIdBuilder.build());
        Set<Contact> contacts = new HashSet<>(Arrays.asList(new Contact(null, null, "Jeff Marshall", "JeffMarshall@atlassian.com"),
                new Contact(address, null, "Recruiting", "recruiting@atlassian.com")));
        accountService.createAccount(contacts, "Atlassian", address, AccountType.ENTERPRISE);
    }

    private void createThirdAccount() {
        AddressId.Builder addressIdBuilder = new AddressId.Builder()
                .line1("Service Now").city("Dallas").state("Texas").country("USA");
        Set<Contact> contacts = new HashSet<>(Arrays.asList(new Contact(null, null, "Joe Anderson", "JoeAnderson@servicenow.com")));
        accountService.createAccount(contacts, "ServiceNow", new Address(addressIdBuilder.build()),
                AccountType.ENTERPRISE);
    }

    private void createFourthAccount() {
        AddressId.Builder addressIdBuilder = new AddressId.Builder()
                .line1("Amazon Web Services").city("Plano").state("Texas").country("USA");
        Set<Contact> contacts = new HashSet<>(Arrays.asList(new Contact(null, null, "Mary Lee", "MaryLee@amazonwebservices.com")));
        accountService.createAccount(contacts, "AWS", new Address(addressIdBuilder.build()),
                AccountType.ENTERPRISE);
    }

    private void createFifthAccount() {
        AddressId.Builder addressIdBuilder = new AddressId.Builder()
                .line1("Number1RobotInc").city("Plano").state("Texas").country("USA");
        Set<Contact> contacts = new HashSet<>(Arrays.asList(new Contact(null, null, "Jamal Wilcox", "jwilcox@robostartup.com")));
        accountService.createAccount(contacts, "RoboStartup", new Address(addressIdBuilder.build()),
                AccountType.SMALL);
    }
}
