package com.atlassian.marketplace.marketplace;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.atlassian.marketplace.marketplace.domain.Address;
import com.atlassian.marketplace.marketplace.domain.AddressId;
import com.atlassian.marketplace.marketplace.service.ContactService;

/**
 * Responsible for persisting Contacts into the DB. A corresponding contact
 * must be persisted before an account can be added.
 */
@Component
public class ContactCreator implements DataCreator {

    private ContactService contactService;
    private static final Logger LOGGER = LoggerFactory.getLogger(ContactCreator.class);

    ContactCreator(ContactService contactService) {
        this.contactService = contactService;
    }

    @Override
    public void createData() {
        AddressId.Builder builder = new AddressId.Builder();
        AddressId addressId = builder.line1("123 curtner drive").city("milpitas").state("california").country("usa").build();
        contactService.createContact("Kevin Cheng", "kevin@workday.com", new Address(addressId));
        contactService.createContact("Henry Kim", "henry@workday.com", new Address(addressId));
        contactService.createContact("VP of Sales", "sales@workday.com", new Address(addressId));
        contactService.createContact("Jeff Marshall", "JeffMarshall@atlassian.com", null);
        contactService.createContact("Recruiting", "recruiting@atlassian.com", null);
        contactService.createContact("John Doe", "JDoe123@workday.com", null);
        contactService.createContact("Mary Lee", "MaryLee@amazonwebservices.com", null);
        contactService.createContact("Joe Anderson", "JoeAnderson@servicenow.com", null);
        contactService.createContact("Jamal Wilcox", "jwilcox@robostartup.com", null);
        LOGGER.info("created " + contactService.total() + " contacts in DB");
    }
}
