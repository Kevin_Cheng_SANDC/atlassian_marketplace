package com.atlassian.marketplace.marketplace;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A facade for creating dummy data for the in memory H2 DB. Only for development/testing!
 */
@Component
public class DummyDataCreatorFacade {
    private AccountCreator accountCreator;
    private ContactCreator contactCreator;

    @Autowired
    DummyDataCreatorFacade(AccountCreator accountCreator, ContactCreator contactCreator) {
        this.accountCreator = accountCreator;
        this.contactCreator = contactCreator;
    }

    public void createDummyData() {
        contactCreator.createData();
        accountCreator.createData();
    }
}
