package com.atlassian.marketplace.marketplace;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarketplaceApplication implements CommandLineRunner {

	@Autowired
	DummyDataCreatorFacade dummyDataCreatorFacade;

	public static void main(String[] args) {
		SpringApplication.run(MarketplaceApplication.class, args);
	}

	/**
	 * Loads fake dummy data into H2 Database. In memory H2 Db is only used for development/testing and not Prod!
	 * Function invoked whenever MarketPlaceApplication starts up
	 */
	private void loadDummyData() {
		dummyDataCreatorFacade.createDummyData();
	}

	@Override
	public void run(String... args) throws Exception {
		loadDummyData();
	}
}
