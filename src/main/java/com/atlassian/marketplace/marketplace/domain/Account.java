package com.atlassian.marketplace.marketplace.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Account {

    @Id
    @Column(length = 100)
    private String companyName;
    @ManyToOne
    private Address address;
    @Column
    @Enumerated
    private AccountType accountType;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "account")
    private Set<Contact> contacts;

    public Account() {

    }

    public Account(String companyName, Address address, AccountType accountType, Set<Contact> contacts) {
        this.companyName = companyName;
        this.address = address;
        this.accountType = accountType;
        this.contacts = contacts;
    }

    @Override
    public String toString() {
        return "Account{" +
                "companyName='" + companyName + '\'' +
                ", address=" + address +
                ", accountType=" + accountType +
                ", contacts=" + contacts +
                '}';
    }

    public Set<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Set<Contact> contacts) {
        this.contacts = contacts;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }
}
