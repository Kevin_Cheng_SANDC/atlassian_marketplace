package com.atlassian.marketplace.marketplace.domain;

import java.util.Arrays;

/**
 * Represents a company's employee size.
 */
public enum AccountType {
    SMALL("small"), MID("mid"), ENTERPRISE("enterprise");
    private String size;

    AccountType(String size) {
        this. size = size;
    }

    public static AccountType findBySize(String size) {
        return Arrays.stream(AccountType.values())
                .filter(accountType -> accountType.size.equalsIgnoreCase(size)).findFirst().orElse(null);
    }
}
