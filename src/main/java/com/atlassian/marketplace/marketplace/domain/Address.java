package com.atlassian.marketplace.marketplace.domain;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.atlassian.marketplace.marketplace.web.AddressDto;

@Entity
public class Address {
    @EmbeddedId
    private AddressId addressId;
    @Column
    private String latitude;
    @Column
    private String longitude;

    public Address(AddressDto addressDto) {
        this.addressId = new AddressId(addressDto.getAddressLine1(), addressDto.getCity(), addressDto.getState(),
                addressDto.getCountry());
    }

    public Address(AddressId addressId) {
        this.addressId = addressId;
    }

    protected Address() {

    }

    public AddressId getAddressId() {
        return addressId;
    }

    public void setAddressId(AddressId addressId) {
        this.addressId = addressId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(addressId, address.addressId) && Objects.equals(latitude, address.latitude) && Objects.equals(longitude, address.longitude);
    }

    @Override
    public int hashCode() {
        return Objects.hash(addressId, latitude, longitude);
    }
}
