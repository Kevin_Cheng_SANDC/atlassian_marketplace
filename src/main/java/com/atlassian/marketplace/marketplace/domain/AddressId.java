package com.atlassian.marketplace.marketplace.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;

/**
 * A Primary Key for a {@link Address}.
 */
@Embeddable
public class AddressId implements Serializable {
    private String line1;
    private String city;
    private String state;
    private String country;

    public AddressId() {

    }

    public static class Builder {
        private String line1;
        private String city;
        private String state;
        private String country;

        public Builder line1(String line1) {
            this.line1 = line1;
            return this;
        }
        public Builder city(String city) {
            this.city = city;
            return this;
        }
        public Builder state(String state) {
            this.state = state;
            return this;
        }
        public Builder country(String country) {
            this.country = country;
            return this;
        }

        public AddressId build() {    // Notice this method
            return new AddressId(this);
        }
    }

    private AddressId(Builder builder) {  // Notice this Constructor
        line1 = builder.line1;
        city = builder.city;
        state = builder.state;
        country = builder.country;
    }

    public AddressId(String line1, String city, String state, String country) {
        this.line1 = line1;
        this.city = city;
        this.state = state;
        this.country = country;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddressId addressId = (AddressId) o;
        return Objects.equals(line1, addressId.line1) && Objects.equals(city, addressId.city) && Objects.equals(state, addressId.state) && Objects.equals(country, addressId.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(line1, city, state, country);
    }

    @Override
    public String toString() {
        return "AddressId{" +
                "line1='" + line1 + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
