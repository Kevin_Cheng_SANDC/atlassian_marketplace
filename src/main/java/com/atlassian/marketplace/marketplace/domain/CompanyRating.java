package com.atlassian.marketplace.marketplace.domain;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

/**
 * Feature enhancement: A user can upload ratings for a company, and get the average score of ratings for a specific
 * company.
 */
@Entity
public class CompanyRating {

    @EmbeddedId
    private CompanyRatingPk pk;
    @Column(nullable = false)
    private Integer score;

    public CompanyRating(CompanyRatingPk pk, Integer score) {
        this.pk = pk;
        this.score = score;
    }

    public CompanyRating() {

    }

    public CompanyRatingPk getPk() {
        return pk;
    }

    public void setPk(CompanyRatingPk pk) {
        this.pk = pk;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "CompanyRating{" +
                "pk=" + pk +
                ", score=" + score +
                '}';
    }
}
