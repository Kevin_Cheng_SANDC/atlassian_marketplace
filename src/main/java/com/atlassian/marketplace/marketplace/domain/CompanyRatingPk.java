package com.atlassian.marketplace.marketplace.domain;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 * The primary key for a {@link CompanyRating}.
 */
@Embeddable
public class CompanyRatingPk implements Serializable {
    @ManyToOne
    private Account account;
    private String userId;

    public CompanyRatingPk(Account account, String userId) {
        this.account = account;
        this.userId = userId;
    }

    public CompanyRatingPk() {

    }

    @Override
    public String toString() {
        return "CompanyRatingPk{" +
                "account=" + account +
                ", userId=" + userId +
                '}';
    }
}
