package com.atlassian.marketplace.marketplace.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
public class Contact implements Serializable {

    @ManyToOne
    private Address address;
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Account account;
    @Column(unique = true)
    private String name;
    @Id
    private String email;

    public Contact(Address address, Account account, String name, String email) {
        this.address = address;
        this.account = account;
        this.name = name;
        this.email = email;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Contact() {
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
