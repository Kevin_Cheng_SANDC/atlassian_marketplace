package com.atlassian.marketplace.marketplace.repo;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.atlassian.marketplace.marketplace.domain.Account;

/**
 * CRUD operations for Accounts. Deletions disabled per technical requirements.
 */
public interface AccountRepository extends CrudRepository<Account, String> {
    Optional<Account> findById(String companyName);
    boolean existsById(String companyName);
    Iterable<Account> findAll();
    Iterable<Account> findAllById(Iterable<String> iterable);
    long count();

    @Override
    @RestResource(exported = false)
    void deleteById(String s);

    @Override
    @RestResource(exported = false)
    void delete(Account entity);

    @Override
    @RestResource(exported = false)
    void deleteAllById(Iterable<? extends String> strings);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Account> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
