package com.atlassian.marketplace.marketplace.repo;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.atlassian.marketplace.marketplace.domain.Address;
import com.atlassian.marketplace.marketplace.domain.AddressId;

/**
 * CRUD operations for Addresses. Deletions disabled per technical requirements.
 */
public interface AddressRepository extends CrudRepository<Address, AddressId>{
    Optional<Address> findById(AddressId addressId);
    boolean existsById(AddressId addressId);
    Iterable<Address> findAll();
    Iterable<Address> findAllById(Iterable<AddressId> iterable);
    long count();

    @Override
    @RestResource(exported = false)
    void deleteById(AddressId addressId);

    @Override
    @RestResource(exported = false)
    void delete(Address entity);

    @Override
    @RestResource(exported = false)
    void deleteAllById(Iterable<? extends AddressId> addressIds);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Address> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}