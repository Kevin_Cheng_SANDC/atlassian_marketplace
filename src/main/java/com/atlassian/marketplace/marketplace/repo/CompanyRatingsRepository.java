package com.atlassian.marketplace.marketplace.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.atlassian.marketplace.marketplace.domain.Account;
import com.atlassian.marketplace.marketplace.domain.CompanyRating;
import com.atlassian.marketplace.marketplace.domain.CompanyRatingPk;

/**
 * Repository is only accessible through {@link com.atlassian.marketplace.marketplace.web.CompanyRatingsController}
 */
@RepositoryRestResource(exported = false)
public interface CompanyRatingsRepository extends CrudRepository<CompanyRating, CompanyRatingPk> {
    List<CompanyRating> findByPkAccount(Account account);
}
