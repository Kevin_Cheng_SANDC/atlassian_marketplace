package com.atlassian.marketplace.marketplace.repo;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.atlassian.marketplace.marketplace.domain.Contact;

/**
 * CRUD operations for Contacts. Deletions disabled per technical requirements.
 */
public interface ContactRepository extends CrudRepository<Contact, String> {
    Optional<Contact> findById(String contactId);
    boolean existsById(String contactId);
    Iterable<Contact> findAll();
    Iterable<Contact> findAllById(Iterable<String> iterable);
    long count();

    @Override
    @RestResource(exported = false)
    void deleteById(String s);

    @Override
    @RestResource(exported = false)
    void delete(Contact entity);

    @Override
    @RestResource(exported = false)
    void deleteAllById(Iterable<? extends String> contactIds);

    @Override
    @RestResource(exported = false)
    void deleteAll(Iterable<? extends Contact> entities);

    @Override
    @RestResource(exported = false)
    void deleteAll();
}
