package com.atlassian.marketplace.marketplace.service;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atlassian.marketplace.marketplace.domain.Address;
import com.atlassian.marketplace.marketplace.domain.Contact;
import com.atlassian.marketplace.marketplace.repo.AddressRepository;
import com.atlassian.marketplace.marketplace.repo.ContactRepository;
import com.atlassian.marketplace.marketplace.web.exceptions.MissingContactException;

@Service
public class ContactService {
    private ContactRepository contactRepository;
    private AddressRepository addressRepository;
    private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private static final Logger LOGGER = LoggerFactory.getLogger(ContactService.class);

    @Autowired
    public ContactService(ContactRepository contactRepository, AddressRepository addressRepository) {
        this.contactRepository = contactRepository;
        this.addressRepository = addressRepository;
    }

    public static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    public void updateContactName(String name, Contact contact) {
        contact.setName(name);
        contactRepository.save(contact);
    }

    public Contact createContact(String name, String email, Address address) {
        Optional<Contact> contactOpt = getContact(email);
        if (contactOpt.isPresent()) {
            return contactOpt.get();
        }
        if (!validate(email)) {
            LOGGER.error("Cannot create contact as email is invalid: " + email);
            throw new MissingContactException("Email address is not valid.");
        }
        if (address != null) {
            addressRepository.save(address);
        }
        Contact contact = new Contact(address, null, name, email);
        contactRepository.save(contact);
        return contact;
    }

    public Iterable<Contact> getAllContacts(Iterable<String> emailIds) {
        return contactRepository.findAllById(emailIds);
    }

    public Optional<Contact> getContact(String email) {
        return contactRepository.findById(email);
    }

    public long total() {
        return contactRepository.count();
    }
}
