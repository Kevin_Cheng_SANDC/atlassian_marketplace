package com.atlassian.marketplace.marketplace.web;

import java.util.List;

public class AccountDto {
    private AddressDto companyAddress;
    private List<ContactDto> contacts;

    public AccountDto(AddressDto companyAddress, List<ContactDto> contacts) {
        this.companyAddress = companyAddress;
        this.contacts = contacts;
    }

    public AddressDto getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(AddressDto companyAddress) {
        this.companyAddress = companyAddress;
    }

    public List<ContactDto> getContacts() {
        return contacts;
    }

    public void setContacts(List<ContactDto> contacts) {
        this.contacts = contacts;
    }
}
