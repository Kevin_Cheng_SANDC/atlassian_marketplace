package com.atlassian.marketplace.marketplace.web;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.atlassian.marketplace.marketplace.domain.Account;
import com.atlassian.marketplace.marketplace.domain.AccountType;
import com.atlassian.marketplace.marketplace.domain.Address;
import com.atlassian.marketplace.marketplace.domain.Contact;
import com.atlassian.marketplace.marketplace.service.AccountService;
import com.atlassian.marketplace.marketplace.service.ContactService;
import com.atlassian.marketplace.marketplace.web.exceptions.AccountAlreadyExistsException;
import com.atlassian.marketplace.marketplace.web.exceptions.AccountDoesNotExistException;
import com.atlassian.marketplace.marketplace.web.exceptions.MissingContactException;

@Validated
@RestController
@RequestMapping(path = "/accounts/{companyName}")
public class AccountsController {
    private AccountService accountService;
    private ContactService contactService;
    private EntityDtoConverter entityDtoConverter;
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountsController.class);

    @Autowired
    public AccountsController(AccountService accountService,
                              ContactService contactService,
                              EntityDtoConverter entityDtoConverter) {
        this.accountService = accountService;
        this.contactService = contactService;
        this.entityDtoConverter = entityDtoConverter;
    }

    /**
     * Get account and contact information for an existing account.
     */
    @GetMapping
    public AccountDto getExistingAccount(@RequestParam(name = "hasContacts", defaultValue = "false") boolean hasContacts,
                                      @PathVariable(value = "companyName") String companyName) {
        Account account = accountService.getExistingAccount(companyName)
                .orElseThrow(() -> new AccountDoesNotExistException("Company " + companyName + " does not exist."));
        return entityDtoConverter.convert(account, hasContacts);
    }

    /**
     * Get the accountType for an existing account.
     */
    @GetMapping("/accountType")
    public AccountType getExistingAccount(@PathVariable(value = "companyName") String companyName) {
        Account account = accountService.getExistingAccount(companyName)
                .orElseThrow(() -> new AccountDoesNotExistException("Company " + companyName + " does not exist."));
        return account.getAccountType();
    }

    /**
     * Update an existing account with the contact information passed in from client.
     */
    @PutMapping("/contacts")
    @ResponseStatus(HttpStatus.CREATED)
    public void addAccountContacts(@PathVariable(value = "companyName") String companyName,
                                                  @RequestBody @Valid List<ContactDto> contactDtoList) {
        if (!accountService.getExistingAccount(companyName).isPresent()) {
            LOGGER.error("Cannot update the account with company name: " + companyName + ". It does not exists.");
            throw new AccountDoesNotExistException("Company " + companyName + " does not exist.");
        }
        accountService.addAccountContacts(contactDtoList,
                companyName);
    }

    /**
     * Update the details for an existing account i.e. longitude, latitude, accountType, etc. Does not modify
     * primary key attributes. See {@link com.atlassian.marketplace.marketplace.domain.AddressId}
     */
    @PutMapping("/details")
    @ResponseStatus(HttpStatus.CREATED)
    public void updateAccountDetails(@PathVariable(value = "companyName") String companyName,
                                     AddressDto addressDto) {
        if (!accountService.getExistingAccount(companyName).isPresent()) {
            LOGGER.error("Cannot update the account with company name: " + companyName + ". It does not exists.");
            throw new AccountDoesNotExistException("Company " + companyName + " does not exist.");
        }
        accountService.updateAccountAddress(addressDto,
                companyName);
    }

    /**
     * Create a new account for a company with existing contacts. Method is idempotent.
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createAccountWithExistingContacts(@PathVariable(value = "companyName") String companyName,
                                                  @RequestBody @Valid AccountDto accountDto) {
        if (accountService.getExistingAccount(companyName).isPresent()) {
            LOGGER.error("Cannot create new account with company name: " + companyName + ". It already exists.");
            throw new AccountAlreadyExistsException("Company " + companyName + " already exists.");
        }
        List<String> contactIds = accountDto.getContacts().stream().map(contactDto -> contactDto.getEmailAddress())
                .collect(Collectors.toList());
        Iterable<Contact> contacts = contactService.getAllContacts(contactIds);
        Set<Contact> result = new HashSet<>();
        contacts.forEach(result::add);
        if (result.isEmpty()) {
            throw new MissingContactException("At least one existing contact is required to create an account. " +
                    "Please create a contact first and try again");
        }
        accountService.createAccount(result, companyName, new Address(accountDto.getCompanyAddress()),
                null);
    }

    /**
     * Exception handler if {@link AccountDoesNotExistException} is thrown in this Controller
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(AccountDoesNotExistException.class)
    public String handleADNE(AccountDoesNotExistException ex) {
        return ex.getMessage();
    }

    /**
     * Exception handler if {@link MissingContactException} is thrown in this Controller
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MissingContactException.class)
    public String handleMCE(MissingContactException ex) {
        return ex.getMessage();
    }

    /**
     * Exception handler if {@link AccountAlreadyExistsException} is thrown in this Controller
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(AccountAlreadyExistsException.class)
    public String handleAAEE(AccountAlreadyExistsException ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity handle(javax.validation.ConstraintViolationException constraintViolationException) {
        Set<ConstraintViolation<?>> violations = constraintViolationException.getConstraintViolations();
        String errorMessage = "";
        if (!violations.isEmpty()) {
            StringBuilder builder = new StringBuilder();
            violations.forEach(violation -> builder.append(" " + violation.getMessage()));
            errorMessage = builder.toString();
        } else {
            errorMessage = "ConstraintViolationException occured.";
        }
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

}
