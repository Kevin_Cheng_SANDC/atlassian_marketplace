package com.atlassian.marketplace.marketplace.web;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class CompanyRatingDto {

    @Min(0)
    @Max(5)
    private Integer score;

    @Size(max = 500)
    private String comment;

    private String userId;

    public CompanyRatingDto() {

    }

    public CompanyRatingDto(@Min(0) @Max(5) Integer score, @Size(max = 500) String comment, String userId) {
        this.score = score;
        this.comment = comment;
        this.userId = userId;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
