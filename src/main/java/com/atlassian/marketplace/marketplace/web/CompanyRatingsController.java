package com.atlassian.marketplace.marketplace.web;

import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.atlassian.marketplace.marketplace.domain.Account;
import com.atlassian.marketplace.marketplace.domain.CompanyRating;
import com.atlassian.marketplace.marketplace.domain.CompanyRatingPk;
import com.atlassian.marketplace.marketplace.repo.AccountRepository;
import com.atlassian.marketplace.marketplace.repo.CompanyRatingsRepository;

/**
 * An example of a RESTController for allowing users to find the average of company ratings for a company
 */
@RestController
@RequestMapping(path = "/apps/{companyName}/ratings")
public class CompanyRatingsController {
    private CompanyRatingsRepository companyRatingsRepository;
    private AccountRepository accountRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyRatingsController.class);

    @Autowired
    public CompanyRatingsController(CompanyRatingsRepository companyRatingsRepository,
                                    AccountRepository accountRepository) {
        this.companyRatingsRepository = companyRatingsRepository;
        this.accountRepository = accountRepository;
    }

    /**
     * Create a Company Rating. userId from CompanyRatingDTO must be unique.
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createCompanyRating(@PathVariable(value = "companyName") String companyName,
                                    @RequestBody @Valid CompanyRatingDto companyRatingDto) {
        Account account = verifyCompanyExists(companyName);
        companyRatingsRepository.save(new CompanyRating(
                new CompanyRatingPk(account, companyRatingDto.getUserId()), companyRatingDto.getScore()));
        LOGGER.info("Created company rating for company ",  companyName + " and " + companyRatingDto.getUserId());
    }

    /**
     * Gets the average rating for a company e.g. Workday.
     */
    @GetMapping
    public double getCompanyRatingAverage(@PathVariable(value = "companyName") String companyName) {
        Account account = verifyCompanyExists(companyName);
        return companyRatingsRepository.findByPkAccount(account).stream().mapToInt(CompanyRating::getScore).average()
                .getAsDouble();
    }

    /**
     * Return the account corresponding to the company.
     * @throws NoSuchElementException
     */
    private Account verifyCompanyExists(String companyName)
            throws NoSuchElementException {
        if (!accountRepository.existsById(companyName)) {
            LOGGER.error("company " + companyName + " does not exist in Db.");
            throw new NoSuchElementException("Account does not exist " + companyName);
        }
        return accountRepository.findById(companyName).get();
    }

    /**
     * Exception handler if NoSuchElementException is thrown in this Controller
     *
     * @param ex exception
     * @return Error message String.
     */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoSuchElementException.class)
    public String return400Code(NoSuchElementException ex) {
        return ex.getMessage();
    }
}
