package com.atlassian.marketplace.marketplace.web;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.atlassian.marketplace.marketplace.domain.Account;
import com.atlassian.marketplace.marketplace.domain.CompanyRating;
import com.atlassian.marketplace.marketplace.domain.CompanyRatingPk;
import com.atlassian.marketplace.marketplace.domain.Contact;
import com.atlassian.marketplace.marketplace.service.AccountService;
import com.atlassian.marketplace.marketplace.service.ContactService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(path = "/contacts/")
public class ContactController {

    @Autowired
    private ContactService contactService;
    private ObjectMapper objectMapper = new ObjectMapper();

    @PutMapping("/{emailId}/")
    public String changeName(@PathVariable(value = "emailId") String emailId, @RequestBody String name) {
        Optional<Contact> contact = contactService.getContact(emailId);
        contact.ifPresent(value -> contactService.updateContactName(name, value));
        return contact.isPresent() ? "success" : "emailId does not exist.";
    }

    @GetMapping("/{emailId}/address")
    public String getAddress(@PathVariable(value = "emailId") String emailId)
            throws JsonProcessingException {
        Optional<Contact> contact = contactService.getContact(emailId);
        return contact.isPresent()
                ? objectMapper.writeValueAsString(contact.get().getAddress())
                : "No address found for " + emailId;
    }

    @GetMapping("/{emailId}/account")
    public String getAccount(@PathVariable(value = "emailId") String emailId)
            throws JsonProcessingException {
        Optional<Contact> contact = contactService.getContact(emailId);
        return contact.isPresent()
                ? objectMapper.writeValueAsString(contact.get().getAccount())
                : "No account found for " + emailId;
    }
}
