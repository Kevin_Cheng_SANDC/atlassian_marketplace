package com.atlassian.marketplace.marketplace.web;

import javax.validation.constraints.NotNull;

public class ContactDto {
    @NotNull(message="emailAddress cannot be missing or empty")
    private String emailAddress;
    private AddressDto address;
    @NotNull(message="name cannot be missing or empty")
    private String name;

    public ContactDto() {

    }

    public ContactDto(String emailAddress, String name) {
        this.name = name;
        this.emailAddress = emailAddress;
    }

    public ContactDto(String emailAddress, AddressDto address, String name) {
        this.emailAddress = emailAddress;
        this.address = address;
        this.name = name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
