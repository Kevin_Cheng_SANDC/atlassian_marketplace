package com.atlassian.marketplace.marketplace.web;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.springframework.stereotype.Component;

import com.atlassian.marketplace.marketplace.domain.Account;
import com.atlassian.marketplace.marketplace.domain.AddressId;
import com.atlassian.marketplace.marketplace.domain.Contact;

/**
 * Responsible for converting between a DTO and an Entity model.
 */
@Component
public class EntityDtoConverter {

    public Set<Contact> convert(List<ContactDto> contactDtoList, Account account) {
        Set<Contact> contacts = new HashSet<>();
        for (ContactDto contactDto : contactDtoList) {
            Contact contact = new Contact();
            setIfNotNull(contact::setEmail, contactDto::getEmailAddress);
            setIfNotNull(contact::setName, contactDto::getName);
            contacts.add(contact);
            contact.setAccount(account);
        }
        return contacts;
    }

    public AddressId convert(AddressDto addressDto) {
        //TODO: Add line2 and postal code to builder
        AddressId.Builder builder = new AddressId.Builder();
        return builder.line1(addressDto.getAddressLine1()).city(addressDto.getCity())
                .country(addressDto.getCountry()).state(addressDto.getState()).build();
    }

    public AccountDto convert(Account account, boolean hasContacts) {
        AddressDto addressDto = new AddressDto();
        setIfNotNull(addressDto::setAddressLine1, () -> account.getAddress().getAddressId().getLine1());
        setIfNotNull(addressDto::setCity, () -> account.getAddress().getAddressId().getCity());
        setIfNotNull(addressDto::setState, () -> account.getAddress().getAddressId().getState());
        setIfNotNull(addressDto::setCountry, () -> account.getAddress().getAddressId().getCountry());
        // TODO: Add line2 and postal code :D
        List<ContactDto> contactDtoList = null;
        if (hasContacts) {
            contactDtoList = new ArrayList<>();
            for (Contact contact : account.getContacts()) {
                ContactDto contactDto = new ContactDto(contact.getEmail(), contact.getName());
                contactDtoList.add(contactDto);
                if (contact.getAddress() != null) {
                    contactDto.setAddress(new AddressDto());
                    AddressDto contactAddressDto = contactDto.getAddress();
                    setIfNotNull(contactAddressDto::setAddressLine1, () -> contact.getAddress().getAddressId().getLine1());
                    setIfNotNull(contactAddressDto::setCity, () -> contact.getAddress().getAddressId().getCity());
                    setIfNotNull(contactAddressDto::setState, () -> contact.getAddress().getAddressId().getState());
                    setIfNotNull(contactAddressDto::setCountry, () -> contact.getAddress().getAddressId().getCountry());
                }
            }
        }
        return new AccountDto(addressDto, contactDtoList);
    }

    /**
     * Helper function to initialize DTO or Entity fields.
     */
    private static <T> void setIfNotNull(Consumer<T> consumer, Supplier<T> supplier) {
        T value = supplier.get();
        if (value != null) {
            consumer.accept(value);
        }
    }
}
