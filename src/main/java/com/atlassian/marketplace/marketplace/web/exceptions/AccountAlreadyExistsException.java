package com.atlassian.marketplace.marketplace.web.exceptions;

public class AccountAlreadyExistsException extends RuntimeException {

    public AccountAlreadyExistsException(String msg) {
        super(msg);
    }
}
