package com.atlassian.marketplace.marketplace.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class MissingContactException extends RuntimeException {

    public MissingContactException(String msg) {
        super(msg);
    }
}
